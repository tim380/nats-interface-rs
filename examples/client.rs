extern crate nats_interface_rs;
use futures::executor::block_on;
use futures::future::BoxFuture;
use nats_interface_rs::oes as nats;

/// This is a small example using an oes-custom nats client.
///
/// The creation of the nats client occurs asynchronously,
/// using Rust's `Future` trait. For more on futures and
/// async/.await syntax:
///
/// [Future trait][future]
/// [async/.await][async]
///
/// This example assumes a locally hosted nats server is up and running.
///
/// [future]: https://rust-lang.github.io/async-book/02_execution/02_future.html
/// [async]: https://rust-lang.github.io/async-book/01_getting_started/04_async_await_primer.html
fn main() {
    let nc = nats::NatsClient::<i32>::new(1);
    let mut nc = block_on(nc).expect("Failed to create nats client");

    fn test(_msg: &'_ nats::Subscription) -> BoxFuture<'_, i32> {
        Box::pin(async { 42 })
    }

    let test_string = String::from("test");

    let sub = nc.add_subscription(test_string.clone(), test);
    block_on(sub).expect("Failed to add subscription");

    let handler = nc.handle_subscription(test_string.clone());
    let output = block_on(handler).expect("failed to handle output");
    assert_eq!(42, output);
}
