#![feature(fn_traits)]
pub mod oes {
    pub use async_nats::{Connection, Message, Options, Subscription};
    use futures::future::BoxFuture;
    use std::borrow::Borrow;
    use std::collections::HashMap;
    use std::env;
    use std::io::{Error, ErrorKind};
    use std::pin::Pin;
    use std::rc::Rc;
    use std::result::Result;

    // TODO: implement Subject wrapper type for String.

    /// Custom nats client; contains a reference counter
    /// to a nats connection and two maps that each map a
    /// subject string to either a `Subscription` or
    /// `fn(Message)` (function pointer that accepts a `Message`
    /// as argument).
    pub struct NatsClient<T> {
        _unique_id: i32,
        _conn_address: String,
        conn: Rc<Connection>,
        subscriptions: HashMap<String, Subscription>,
        handlers: HashMap<String, fn(&'_ Subscription) -> BoxFuture<'_, T>>,
    }
    pub type NatsClientRc<T> = Rc<NatsClient<T>>;

    impl<T> NatsClient<T> {
        /// Create a `NatsClient` asynchronously.
        /// the use of `.await` prevents the calling
        /// thread from blocking, and allows other threads
        /// to make progress if the `Future` returned by
        /// `connect()` is not complete.
        pub async fn new(uid: i32) -> Result<Self, Error> {
            let _conn_address = String::from("nats://localhost:4222");

            let conn = if std::path::Path::new("/etc/nats/user.creds").exists() {
                Rc::new(
                    Options::with_credentials("/etc/nats/user.creds")
                        .connect(&_conn_address[..])
                        .await?,
                )
            } else {
                Rc::new(async_nats::connect(&_conn_address[..]).await?)
            };

            Ok(NatsClient {
                _unique_id: uid,
                _conn_address,
                conn,
                subscriptions: Default::default(),
                handlers: Default::default(),
            })
        }

        pub fn generate_local_connection_string() -> String {
            let ip = env::var("PLATFORM_NODE_IP");
            let port = env::var("PLATFORM_NATS_PORT");
            let service = env::var("PLATFORM_NATS_SERVICE");

            if service.is_ok() {
                let service = service.unwrap();
                println!("Local nats via service environment: {}", service);
                return service;
            }

            if ip.is_ok() && port.is_ok() {
                let mut address = ip.unwrap();
                let port = port.unwrap();
                address.push(':');
                address.push_str(&port[..]);
                println!("Local nats via ip and port environment: {}", address);
                return address;
            }

            let last_resort = String::from("nats://0.0.0.4222");
            println!("Local nats via last resort: {}", last_resort);
            last_resort
        }

        pub fn match_wild_cards(wild: char, normal: char) -> bool {
            todo!()
        }

        pub async fn handle_subscription(&self, sub: String) -> Result<T, Error> {
            let handle = self.handlers.get(&sub);
            let subscription = self.subscriptions.get(&sub);
            if handle.is_some() && subscription.is_some() {
                let handle = handle.unwrap();
                let subscription = subscription.unwrap();
                Ok(handle(&subscription).await)
            } else {
                let e = Error::from(ErrorKind::NotFound);
                Err(e)
            }
        }

        pub async fn add_subscription(
            &mut self,
            subject: String,
            handle: fn(&'_ Subscription) -> BoxFuture<'_, T>,
        ) -> Result<(), Error> {
            let sub = self.conn.subscribe(&subject[..]).await?;
            self.subscriptions.insert(subject.clone(), sub);
            self.handlers.insert(subject, handle);
            Ok(())
        }

        pub async fn remove_subscription(&mut self, subject: String) -> Result<(), Error> {
            let subscr = self.subscriptions.remove(&subject);
            let handle = self.handlers.remove(&subject);
            if subscr.is_none() || handle.is_none() {
                return Err(Error::from(ErrorKind::NotFound));
            }
            let subscr = subscr.unwrap();
            Ok(subscr.unsubscribe().await?)
        }

        pub async fn publish_message(
            &self,
            subj: String,
            msg: String,
            reply_subj: Option<String>,
        ) -> Result<(), Error> {
            let conn: &Connection = self.conn.borrow();
            match reply_subj {
                Some(reply_subj) => Ok(conn
                    .publish_request(&subj[..], &reply_subj[..], msg.as_bytes())
                    .await?),
                None => Ok(conn.publish(&subj, msg.as_bytes()).await?),
            }
        }

        pub async fn request_message(
            &self,
            subj: String,
            msg: String,
            timeout: u64,
        ) -> Result<Message, Error> {
            let timeout = std::time::Duration::new(timeout, 0);
            let conn: &Connection = self.conn.borrow();
            Ok(conn
                .request_timeout(&subj[..], msg.as_bytes(), timeout)
                .await?)
        }

        pub fn print_handler(msg: Message) {
            todo!()
        }
    }
}
